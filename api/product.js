module.exports = app => {
    const { existsOrError, notExistsOrError} = app.api.validation

    const save = async (req, res) => {
        let product = { ...req.body }
        const sizes = JSON.parse(JSON.stringify(product.sizes)); 
        delete product.sizes
        for(let i=0; i < sizes.length; i++) {
            try {
                existsOrError(product.name, 'Nome não informado.')
                existsOrError(product.ref, 'Referência não informada.')
                existsOrError(product.price, 'Preço unitário não informado.')
                existsOrError(product.description, 'Descrição não informada.')
                existsOrError(product.imageUrl, 'URL da imagem não informado.')   
                let checkRef = await errorCheckingRef(product)
                if(checkRef) notExistsOrError(product, 'Referência já cadastrada. Revise o nome do produto.')
                product.size_id = sizes[i].idSize
                existsOrError(product.size_id, 'Tamanho não informado.') 
                product.quantityInStock = sizes[i].quantityInStock
                existsOrError(product.quantityInStock, 'Quantidade em estoque não informada.')
                let checkSize = await errorCheckingSize(product)
                if(checkSize) notExistsOrError(product, 'Tamanho do produto já cadastrado, atualize o registro.')       
            } catch(msg) {
                return res.status(400).send(msg)
            }
            await app.db('product')
                .insert(product)
                .catch(err => res.status(500).send(err))
        }
        res.status(204).send()
    }

    const errorCheckingRef = async (product) => {
        const productRefFromDB = await app.db('product')
            .where({ ref: product.ref}).first()    
        if(!productRefFromDB) return false
        if(product.name !== productRefFromDB.name) return true
    }

    const errorCheckingSize = async (product) => {
        const productsRefFromDB = await app.db('product')
            .where({ ref: product.ref})
        if(productsRefFromDB.length === 0) return false
        const check = productsRefFromDB.find( productDb => productDb.size_id === product.size_id)  
        if(check) return true
    }

    updateProduct = async (req, res) => {
        const product = { ...req.body }
        product.id = req.params.id
        try {
            existsOrError(product.id, 'Código não informado.')
            existsOrError(product.name, 'Nome não informado.')
            existsOrError(product.ref, 'Referência não informada.')
            existsOrError(product.price, 'Preço unitário não informado.')
            existsOrError(product.quantityInStock, 'Quantidade em estoque não informada.')
            existsOrError(product.description, 'Descrição não informada.')
            existsOrError(product.imageUrl, 'URL da imagem não informado.')   
            notExistsOrError(product.size_id, 'Não altere o tamanho')
        } catch(msg) {
            return res.status(400).send(msg)
        }
        app.db('product')
            .update(product)
            .where({id: product.id})
            .then( () => res.status(204).send() )
            .catch(err => res.status(500).send(err))
    }

    const get = (req, res) => {
        app.db('product')
            .select()
            .then(products =>res.json(products))
            .catch(err => res.status(500).send(err))
    }

    const getById = async (req, res) => {
        const productId = req.params.id
        try{
            existsOrError(productId, 'Id não informado')
            const productFromDB = await app.db('product')
                .where({ id: productId}).first()
            existsOrError(productFromDB, 'Produto não encontrado.')
        } catch(msg){
            return res.status(400).send(msg)
        }
        app.db('product')
            .select().first()
            .where({id: productId})
            .then( product => res.json(product))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        const productId = req.params.id
        try{
            existsOrError(productId, 'Id não informado')
            const productFromDB = await app.db('product')
                .where({ id: productId}).first()
            existsOrError(productFromDB, 'Produto não encontrado.')
        } catch(msg){
            return res.status(400).send(msg)
        }
        app.db('product')
            .del()
            .where({id: productId})
            .then( product => res.json(product))
            .catch(err => res.status(500).send(err))
    }

    const getAllByRef = async (req, res) => {
        const allRefs =
        await app.db('product')
            .select()
            .distinct()
            .pluck('ref')
            .then(states => states)
            .catch(err => res.status(500).send(err))

        const productsByRef = []
        for(let i=0; i < allRefs.length; i++) {
            await app.db('product')
            .select()
            .where({ref: allRefs[i]})
            .then(products => productsByRef.push(products))
            .catch(err => res.status(500).send(err))
        }
        res.status(200).json(productsByRef)
    }

    const getOneByRef = async (req, res) => {
        const allRefs =
        await app.db('product')
            .select()
            .distinct()
            .pluck('ref')
            .then(states => states)
            .catch(err => res.status(500).send(err))

        const productsByRef = []
        for(let i=0; i < allRefs.length; i++) {
            await app.db('product')
            .select()
            .where({ref: allRefs[i]})
            .then(products => {
                const filterProduct = products.filter(product => product.quantityInStock > 0)
                if(filterProduct.length > 0) productsByRef.push(filterProduct)     
            })
            .catch(err => res.status(500).send(err))
        }
        const oneByRef = []
        productsByRef.forEach(array => oneByRef.push(array[0]))
        res.status(200).json(oneByRef)
    }

    const showAllByRef = async (req, res) => {
        const allRefs =
        await app.db('product')
            .select()
            .distinct()
            .pluck('ref')
            .then(states => states)
            .catch(err => res.status(500).send(err))

        const productsByRef = []
        for(let i=0; i < allRefs.length; i++) {
            await app.db('product')
            .select()
            .where({ref: allRefs[i]})
            .then(products => {
                const filterProduct = products.filter(product => product.quantityInStock > 0)
                if(filterProduct.length > 0) productsByRef.push(filterProduct)     
            })
            .catch(err => res.status(500).send(err))
        }
        
        res.status(200).json(productsByRef)
    }

    const getProductByRef = async (req, res) => {
        const productRef = req.params.ref
        try{
            existsOrError(productRef, 'Referência não informada')
            const productFromDB = await app.db('product')
                .where({ ref: productRef}).first()
            existsOrError(productFromDB, 'Produto não encontrado.')
        } catch(msg){
            return res.status(400).send(msg)
        }

        const productsFilter = 
        await app.db('product')
            .select()
            .orderBy('id', 'desc')
            .where({ref: productRef})
            .then( product => product )
            .catch(err => res.status(500).send(err))

        productsFilter.forEach(product => {    
            switch(product.size_id){
                case 1: 
                    product.sizeName = 'PP' 
                    break
                case 2: 
                    product.sizeName = 'P' 
                    break
                case 3: 
                    product.sizeName = 'M' 
                    break
                case 4: 
                    product.sizeName = 'G' 
                    break
                case 5: 
                    product.sizeName = 'GG' 
                    break
                case 6: 
                    product.sizeName = 'XG' 
                    break
            }
        })
            
        res.json(productsFilter)
    }

    return { save, get, getById, remove, updateProduct, getAllByRef, getProductByRef, showAllByRef, getOneByRef }
}