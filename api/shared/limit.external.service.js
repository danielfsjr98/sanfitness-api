const axios = require("axios");

module.exports = (app) => {
  async function getLimitByDocument(document) {
    const options = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ''`,
      },
    };
    const endpoint = `http://ec2-54-94-154-227.sa-east-1.compute.amazonaws.com:7000/api/v1/credito?cpf=${document}`;

    return axios
      .get(endpoint, options)
      .then((res) => {
        return res?.data?.limite || 0;
      })
      .catch((err) => {
        throw err;
      });
  }

  return {getLimitByDocument}
};
