module.exports = (app) => {
  const { existsOrError, notExistsOrError, throwError } = app.api.validation;

  const save = async (req, res) => {
    const salesItems = Object.values({ ...req.body.sales });
    const userId = req.body.userId;

    let userLimit = 0;

    try {
      existsOrError(salesItems, "Pedido não informado");
      existsOrError(userId, "Usuário não informado.");
      const userFromDB = await app.db("users").where({ id: userId }).first();

      if (!userFromDB.reseller && !userFromDB.admin)
        notExistsOrError(
          userFromDB,
          "Usuário não tem permissão para solicitar pedido."
        );

      userLimit = userFromDB.limit;
    } catch (msg) {
      return res.status(400).send(msg);
    }

    const totalSale = salesItems
      .map((sale) => sale.price * sale.amount)
      .reduce(
        (accumulator, currentValue) =>
          parseFloat(accumulator) + parseFloat(currentValue)
      );

    try {
      await validateUserLimit(userLimit, totalSale, userId);
    } catch (msg) {
      return res.status(400).send(msg);
    }

    const sale = { total_sale: totalSale, userId: userId, fulfilled: false };

    app
      .db("sales")
      .insert(sale, "id")
      .then(async (id) => {
        for (let i = 0; i < salesItems.length; i++) {
          const payload = {
            userId,
            unit_price: parseFloat(salesItems[i].price),
            amount: parseFloat(salesItems[i].amount),
            sales_id: id[0],
            product_id: salesItems[i].id,
            total_price:
              parseFloat(salesItems[i].amount) *
              parseFloat(salesItems[i].price),
          };
          // verificar quantidade máxima de produto.
          const productFromDB = await app
            .db("product")
            .where({ id: payload.product_id })
            .first();
          if (productFromDB.quantityInStock < payload.amount)
            notExistsOrError(
              payload.amount,
              `Pedido do produto REF: ${productFromDB.ref} maior do que o disponível.`
            );
          //fim verificação
          await app
            .db("sales_item")
            .insert(payload)
            .then(() => decreaseStock(productFromDB.id, payload.amount));
        }
        return (resp = { id: id[0], total_sale: sale.total_sale });
      })
      .then((resp) => res.json(resp))
      .catch((err) => res.status(500).send(err));
  };

  const validateUserLimit = async (limit, salesAmount, userId) => {
    const result = await app
      .db("sales")
      .where({ userId, fulfilled: false })
      .sum("total_sale")
      .first();

    const userOpenSaleAmount = result?.sum || 0;

    if (userOpenSaleAmount + salesAmount > limit) {
      throwError("Pedido negado. O valor de seus pedidos em aberto ficará maior do que seu limite.");
    }
  };

  const decreaseStock = async (idProduct, quantitySales) => {
    const productFromDB = await app
      .db("product")
      .where({ id: idProduct })
      .first();
    productFromDB.quantityInStock =
      productFromDB.quantityInStock - quantitySales;
    await app
      .db("product")
      .update(productFromDB)
      .where({ id: idProduct })
      .catch((err) => res.status(500).send(err));
  };

  const updateSale = async (req, res) => {
    const sale = { ...req.body };
    try {
      existsOrError(sale.total_sale, "Total não informado.");
      existsOrError(sale.userId, "Usuário não informado");
      const saleFromDB = await app.db("sales").where({ id: sale.id }).first();
      sale.sale_date = saleFromDB.sale_date;
    } catch (e) {
      return res.status(400).send(e);
    }
    await app
      .db("sales")
      .update(sale)
      .where({ id: sale.id })
      .then(() => res.status(204).send())
      .catch((err) => res.status(500).send(err));
  };

  const get = (req, res) => {
    app
      .db("sales")
      .select()
      .orderBy("id", "desc")
      .then((sales) => res.json(sales))
      .catch((err) => res.status(500).send(err));
  };

  const getById = async (req, res) => {
    const saleId = req.params.id;
    try {
      existsOrError(saleId, "Id não informado");
      const saleFromDB = await app.db("sales").where({ id: saleId }).first();
      existsOrError(saleFromDB, "Pedido não encontrado.");
    } catch (msg) {
      return res.status(400).send(msg);
    }

    app
      .db("sales")
      .select()
      .first()
      .where({ id: saleId })
      .then((sale) => res.json(sale))
      .catch((err) => res.status(500).send(err));
  };

  const remove = async (req, res) => {
    const saleId = req.params.id;
    try {
      existsOrError(saleId, "Id não informado");
      const saleFromDB = await app.db("sales").where({ id: saleId }).first();
      existsOrError(saleFromDB, "Pedido não encontrado.");
    } catch (msg) {
      return res.status(400).send(msg);
    }
    app
      .db("sales")
      .del()
      .where({ id: saleId })
      .then((sale) => res.json(sale))
      .catch((err) => res.status(500).send(err));
  };

  const getSalesItemBySaleId = async (req, res) => {
    const saleId = req.params.id;
    app
      .db("sales_item")
      .select()
      .where({ sales_id: saleId })
      .then((saleItem) => res.json(saleItem))
      .catch((err) => res.status(500).send(err));
  };

  const limit = 6;

  const getSalesByUserId = async (req, res) => {
    const page = req.query.page || 1;
    const userId = req.params.id;
    const result = await app
      .db("sales")
      .where({ userId: userId })
      .count("id")
      .first();
    const count = parseInt(result.count);
    app
      .db("sales")
      .select()
      .orderBy("id", "desc")
      .limit(limit)
      .offset(page * limit - limit)
      .where({ userId: userId })
      .then((saleItem) => res.json({ data: saleItem, count, limit }))
      .catch((err) => res.status(500).send(err));
  };

  return {
    save,
    get,
    getById,
    remove,
    getSalesItemBySaleId,
    getSalesByUserId,
    updateSale,
  };
};
