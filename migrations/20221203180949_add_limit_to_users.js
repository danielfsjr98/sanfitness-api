exports.up = function (knex) {
  return knex.schema.table("users", function (table) {
    table.decimal("limit", 14, 2).notNull().defaultTo(0);
  });
};

exports.down = function (knex, Promise) {
  return knex.schema.table("users", function (table) {
    table.dropColumn("limit");
  });
};
